package de.ble.qrcode.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShortUrlRequest {
    private String url;
}
