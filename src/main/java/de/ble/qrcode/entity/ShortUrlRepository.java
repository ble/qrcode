package de.ble.qrcode.entity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ShortUrlRepository extends JpaRepository<ShortUrl, Long> {
    ShortUrl findByKey(String key);
    ShortUrl findByFullUrl(String fullUrl);
}
