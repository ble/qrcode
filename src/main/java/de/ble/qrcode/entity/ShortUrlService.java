package de.ble.qrcode.entity;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.RedirectView;

import de.ble.qrcode.dto.ShortUrlRequest;
import de.ble.qrcode.dto.ShortUrlResponse;
import de.ble.qrcode.util.ShortUrlUtil;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ShortUrlService {

    private final ShortUrlRepository repository;
    private final ShortUrlUtil util;

    @Transactional
    public ShortUrlResponse createShortUrl(ShortUrlRequest request) {
        String fullUrl = request.getUrl();

        ShortUrl existingShortUrl = repository.findByFullUrl(fullUrl);

        if (existingShortUrl != null) {
            return ShortUrlResponse.builder().key(existingShortUrl.getKey()).build();
        } else {
            String newKey = util.generateUniqueKey();
            var newEntity = ShortUrl.builder()
                    .key(newKey).fullUrl(fullUrl).clickCount(0L)
                    .build();
            if (newEntity != null) {
                repository.save(newEntity);
                return ShortUrlResponse.builder().key(newKey).build();
            }
            return null;
        }
    }

    @Transactional
    public RedirectView getFullUrl(String key) {
        ShortUrl entityInDb = repository.findByKey(key);
        if (entityInDb != null) {
            entityInDb.setClickCount(entityInDb.getClickCount() + 1);
            repository.save(entityInDb);
            String url = "" + entityInDb.getFullUrl();
            return new RedirectView(url, false);
        }
        return null;
    }

    @Transactional
    public ShortUrl getUrl(String key) {
        return repository.findByKey(key);
    }

    @Transactional
    public void setUrl(ShortUrl url) {
        if (url != null) {
            repository.save(url);
        }
    }
}
