package de.ble.qrcode.entity;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import de.ble.qrcode.config.ShortUrlConfig;
import de.ble.qrcode.dto.ShortUrlRequest;
import de.ble.qrcode.dto.ShortUrlResponse;
import de.ble.qrcode.util.BarcodeUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ShortUrlController {

    private ShortUrlService service;
    private ShortUrlConfig config;

    public ShortUrlController(ShortUrlService service, ShortUrlConfig config) {
        this.service = service;
        this.config = config;
    }

    @GetMapping("/")
    String index() {
        return "index";
    }

    @GetMapping("/edit")
    String edit(@RequestParam("shorturl") String shorturl, Model model) {
        model.addAttribute("url", service.getUrl(shorturl));
        return "edit";
    }

    @PostMapping("/edit")
    String safeedit(@RequestParam("url") String url, @RequestParam("shorturl") String shorturl) {
        ShortUrl myUrl = service.getUrl(shorturl);
        if (myUrl != null) {
            myUrl.setFullUrl(url);
            service.setUrl(myUrl);
        } else {
            ShortUrlRequest myRequest = new ShortUrlRequest();
            myRequest.setUrl(url);
            service.createShortUrl(myRequest);
        }

        return "index";
    }

    @PostMapping("/")
    String url(@RequestParam("url") String url, Model model) {
        ShortUrlRequest r = new ShortUrlRequest();
        r.setUrl(url);
        ShortUrlResponse response = service.createShortUrl(r);
        model.addAttribute("url", url);
        model.addAttribute("shorturl", config.getUrl() + response.getKey());
        try {
            model.addAttribute("qrcode", BarcodeUtil.generateQRCodeImage(config.getUrl() + response.getKey()));
            model.addAttribute("key", response.getKey());
        } catch (Exception e) {
            log.error("Barcode konnte nicht erzeugt werden.", e);
        }
        return "shorturl";
    }

    @GetMapping("/p")
    RedirectView original(@RequestParam("key") String shorturl) {
        return service.getFullUrl(shorturl);

    }

}
