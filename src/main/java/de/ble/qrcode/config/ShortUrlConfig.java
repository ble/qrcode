package de.ble.qrcode.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(prefix = "short-url")
@Getter
@Setter
@Component
public class ShortUrlConfig {
    private String allowedCharacters;
    private int keyLength;
    private String url; 
}
